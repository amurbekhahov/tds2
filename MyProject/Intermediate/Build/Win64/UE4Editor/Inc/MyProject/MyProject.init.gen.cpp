// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyProject_init() {}
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MyProject()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/MyProject",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x82317309,
				0x84DB1FD5,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
