// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UDecalComponent;
class UAnimMontage;
struct FAdditionalWeaponInfo;
class AWeaponDefault;
#ifdef MYPROJECT_MyProjectCharacter_generated_h
#error "MyProjectCharacter.generated.h already included, missing '#pragma once' in MyProjectCharacter.h"
#endif
#define MYPROJECT_MyProjectCharacter_generated_h

#define MyProject_Source_MyProject_MyProjectCharacter_h_17_SPARSE_DATA
#define MyProject_Source_MyProject_MyProjectCharacter_h_17_RPC_WRAPPERS \
	virtual void WeaponFireStart_BP_Implementation(UAnimMontage* Anim); \
	virtual void WeaponReloadEnd_BP_Implementation(bool bIsSuccess); \
	virtual void WeaponReloadStart_BP_Implementation(UAnimMontage* Anim); \
 \
	DECLARE_FUNCTION(execGetCursorToWorld); \
	DECLARE_FUNCTION(execWeaponFireStart_BP); \
	DECLARE_FUNCTION(execWeaponFireStart); \
	DECLARE_FUNCTION(execWeaponReloadEnd_BP); \
	DECLARE_FUNCTION(execWeaponReloadStart_BP); \
	DECLARE_FUNCTION(execWeaponReloadEnd); \
	DECLARE_FUNCTION(execWeaponReloadStart); \
	DECLARE_FUNCTION(execTryReloadWeapon); \
	DECLARE_FUNCTION(execRemoveCurrentWeapon); \
	DECLARE_FUNCTION(execInitWeapon); \
	DECLARE_FUNCTION(execGetCurrentWeapon); \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execAttackCharEvent); \
	DECLARE_FUNCTION(execMovementTick); \
	DECLARE_FUNCTION(execInputAttackReleased); \
	DECLARE_FUNCTION(execInputAttackPressed); \
	DECLARE_FUNCTION(execInputAxisX); \
	DECLARE_FUNCTION(execInputAxisY);


#define MyProject_Source_MyProject_MyProjectCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void WeaponFireStart_BP_Implementation(UAnimMontage* Anim); \
	virtual void WeaponReloadEnd_BP_Implementation(bool bIsSuccess); \
	virtual void WeaponReloadStart_BP_Implementation(UAnimMontage* Anim); \
 \
	DECLARE_FUNCTION(execGetCursorToWorld); \
	DECLARE_FUNCTION(execWeaponFireStart_BP); \
	DECLARE_FUNCTION(execWeaponFireStart); \
	DECLARE_FUNCTION(execWeaponReloadEnd_BP); \
	DECLARE_FUNCTION(execWeaponReloadStart_BP); \
	DECLARE_FUNCTION(execWeaponReloadEnd); \
	DECLARE_FUNCTION(execWeaponReloadStart); \
	DECLARE_FUNCTION(execTryReloadWeapon); \
	DECLARE_FUNCTION(execRemoveCurrentWeapon); \
	DECLARE_FUNCTION(execInitWeapon); \
	DECLARE_FUNCTION(execGetCurrentWeapon); \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execAttackCharEvent); \
	DECLARE_FUNCTION(execMovementTick); \
	DECLARE_FUNCTION(execInputAttackReleased); \
	DECLARE_FUNCTION(execInputAttackPressed); \
	DECLARE_FUNCTION(execInputAxisX); \
	DECLARE_FUNCTION(execInputAxisY);


#define MyProject_Source_MyProject_MyProjectCharacter_h_17_EVENT_PARMS \
	struct MyProjectCharacter_eventWeaponFireStart_BP_Parms \
	{ \
		UAnimMontage* Anim; \
	}; \
	struct MyProjectCharacter_eventWeaponReloadEnd_BP_Parms \
	{ \
		bool bIsSuccess; \
	}; \
	struct MyProjectCharacter_eventWeaponReloadStart_BP_Parms \
	{ \
		UAnimMontage* Anim; \
	};


#define MyProject_Source_MyProject_MyProjectCharacter_h_17_CALLBACK_WRAPPERS
#define MyProject_Source_MyProject_MyProjectCharacter_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyProjectCharacter(); \
	friend struct Z_Construct_UClass_AMyProjectCharacter_Statics; \
public: \
	DECLARE_CLASS(AMyProjectCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(AMyProjectCharacter)


#define MyProject_Source_MyProject_MyProjectCharacter_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAMyProjectCharacter(); \
	friend struct Z_Construct_UClass_AMyProjectCharacter_Statics; \
public: \
	DECLARE_CLASS(AMyProjectCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(AMyProjectCharacter)


#define MyProject_Source_MyProject_MyProjectCharacter_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyProjectCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyProjectCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyProjectCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyProjectCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyProjectCharacter(AMyProjectCharacter&&); \
	NO_API AMyProjectCharacter(const AMyProjectCharacter&); \
public:


#define MyProject_Source_MyProject_MyProjectCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyProjectCharacter(AMyProjectCharacter&&); \
	NO_API AMyProjectCharacter(const AMyProjectCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyProjectCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyProjectCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyProjectCharacter)


#define MyProject_Source_MyProject_MyProjectCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(AMyProjectCharacter, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AMyProjectCharacter, CameraBoom); }


#define MyProject_Source_MyProject_MyProjectCharacter_h_14_PROLOG \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_EVENT_PARMS


#define MyProject_Source_MyProject_MyProjectCharacter_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_SPARSE_DATA \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_RPC_WRAPPERS \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_CALLBACK_WRAPPERS \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_INCLASS \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject_Source_MyProject_MyProjectCharacter_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_SPARSE_DATA \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_CALLBACK_WRAPPERS \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_INCLASS_NO_PURE_DECLS \
	MyProject_Source_MyProject_MyProjectCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class AMyProjectCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject_Source_MyProject_MyProjectCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
