// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDropItem;
struct FWeaponInfo;
#ifdef MYPROJECT_TPSGameInstance_generated_h
#error "TPSGameInstance.generated.h already included, missing '#pragma once' in TPSGameInstance.h"
#endif
#define MYPROJECT_TPSGameInstance_generated_h

#define MyProject_Source_MyProject_TPSGameInstance_h_19_SPARSE_DATA
#define MyProject_Source_MyProject_TPSGameInstance_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDropItemInfoByName); \
	DECLARE_FUNCTION(execGetDropItemInfoByWeaponName); \
	DECLARE_FUNCTION(execGetWeaponInfoByName);


#define MyProject_Source_MyProject_TPSGameInstance_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDropItemInfoByName); \
	DECLARE_FUNCTION(execGetDropItemInfoByWeaponName); \
	DECLARE_FUNCTION(execGetWeaponInfoByName);


#define MyProject_Source_MyProject_TPSGameInstance_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTPSGameInstance(); \
	friend struct Z_Construct_UClass_UTPSGameInstance_Statics; \
public: \
	DECLARE_CLASS(UTPSGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(UTPSGameInstance)


#define MyProject_Source_MyProject_TPSGameInstance_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUTPSGameInstance(); \
	friend struct Z_Construct_UClass_UTPSGameInstance_Statics; \
public: \
	DECLARE_CLASS(UTPSGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(UTPSGameInstance)


#define MyProject_Source_MyProject_TPSGameInstance_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTPSGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTPSGameInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTPSGameInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTPSGameInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTPSGameInstance(UTPSGameInstance&&); \
	NO_API UTPSGameInstance(const UTPSGameInstance&); \
public:


#define MyProject_Source_MyProject_TPSGameInstance_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTPSGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTPSGameInstance(UTPSGameInstance&&); \
	NO_API UTPSGameInstance(const UTPSGameInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTPSGameInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTPSGameInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTPSGameInstance)


#define MyProject_Source_MyProject_TPSGameInstance_h_19_PRIVATE_PROPERTY_OFFSET
#define MyProject_Source_MyProject_TPSGameInstance_h_15_PROLOG
#define MyProject_Source_MyProject_TPSGameInstance_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_TPSGameInstance_h_19_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_TPSGameInstance_h_19_SPARSE_DATA \
	MyProject_Source_MyProject_TPSGameInstance_h_19_RPC_WRAPPERS \
	MyProject_Source_MyProject_TPSGameInstance_h_19_INCLASS \
	MyProject_Source_MyProject_TPSGameInstance_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject_Source_MyProject_TPSGameInstance_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_TPSGameInstance_h_19_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_TPSGameInstance_h_19_SPARSE_DATA \
	MyProject_Source_MyProject_TPSGameInstance_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject_Source_MyProject_TPSGameInstance_h_19_INCLASS_NO_PURE_DECLS \
	MyProject_Source_MyProject_TPSGameInstance_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class UTPSGameInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject_Source_MyProject_TPSGameInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
