// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MyProject/TPSInventoryComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTPSInventoryComponent() {}
// Cross Module References
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MyProject();
	MYPROJECT_API UScriptStruct* Z_Construct_UScriptStruct_FWeaponSlot();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature();
	MYPROJECT_API UEnum* Z_Construct_UEnum_MyProject_EWeaponType();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature();
	MYPROJECT_API UScriptStruct* Z_Construct_UScriptStruct_FAdditionalWeaponInfo();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature();
	MYPROJECT_API UClass* Z_Construct_UClass_UTPSInventoryComponent_NoRegister();
	MYPROJECT_API UClass* Z_Construct_UClass_UTPSInventoryComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	MYPROJECT_API UScriptStruct* Z_Construct_UScriptStruct_FDropItem();
	MYPROJECT_API UScriptStruct* Z_Construct_UScriptStruct_FAmmoSlot();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics
	{
		struct _Script_MyProject_eventOnUpdateWeaponSlots_Parms
		{
			int32 IndexSlotChange;
			FWeaponSlot NewInfo;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IndexSlotChange;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics::NewProp_IndexSlotChange = { "IndexSlotChange", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnUpdateWeaponSlots_Parms, IndexSlotChange), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics::NewProp_NewInfo = { "NewInfo", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnUpdateWeaponSlots_Parms, NewInfo), Z_Construct_UScriptStruct_FWeaponSlot, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics::NewProp_IndexSlotChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics::NewProp_NewInfo,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MyProject, nullptr, "OnUpdateWeaponSlots__DelegateSignature", nullptr, nullptr, sizeof(_Script_MyProject_eventOnUpdateWeaponSlots_Parms), Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics
	{
		struct _Script_MyProject_eventOnWeaponAmmoAviable_Parms
		{
			EWeaponType WeaponType;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_WeaponType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_WeaponType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics::NewProp_WeaponType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics::NewProp_WeaponType = { "WeaponType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnWeaponAmmoAviable_Parms, WeaponType), Z_Construct_UEnum_MyProject_EWeaponType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics::NewProp_WeaponType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics::NewProp_WeaponType,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MyProject, nullptr, "OnWeaponAmmoAviable__DelegateSignature", nullptr, nullptr, sizeof(_Script_MyProject_eventOnWeaponAmmoAviable_Parms), Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics
	{
		struct _Script_MyProject_eventOnWeaponAmmoEmpty_Parms
		{
			EWeaponType WeaponType;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_WeaponType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_WeaponType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics::NewProp_WeaponType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics::NewProp_WeaponType = { "WeaponType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnWeaponAmmoEmpty_Parms, WeaponType), Z_Construct_UEnum_MyProject_EWeaponType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics::NewProp_WeaponType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics::NewProp_WeaponType,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MyProject, nullptr, "OnWeaponAmmoEmpty__DelegateSignature", nullptr, nullptr, sizeof(_Script_MyProject_eventOnWeaponAmmoEmpty_Parms), Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics
	{
		struct _Script_MyProject_eventOnWeaponAdditionalInfoChange_Parms
		{
			int32 IndexSlot;
			FAdditionalWeaponInfo AdditionalInfo;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IndexSlot;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdditionalInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics::NewProp_IndexSlot = { "IndexSlot", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnWeaponAdditionalInfoChange_Parms, IndexSlot), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics::NewProp_AdditionalInfo = { "AdditionalInfo", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnWeaponAdditionalInfoChange_Parms, AdditionalInfo), Z_Construct_UScriptStruct_FAdditionalWeaponInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics::NewProp_IndexSlot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics::NewProp_AdditionalInfo,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MyProject, nullptr, "OnWeaponAdditionalInfoChange__DelegateSignature", nullptr, nullptr, sizeof(_Script_MyProject_eventOnWeaponAdditionalInfoChange_Parms), Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics
	{
		struct _Script_MyProject_eventOnAmmoChange_Parms
		{
			EWeaponType TypeAmmo;
			int32 Cout;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TypeAmmo_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TypeAmmo;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Cout;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::NewProp_TypeAmmo_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::NewProp_TypeAmmo = { "TypeAmmo", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnAmmoChange_Parms, TypeAmmo), Z_Construct_UEnum_MyProject_EWeaponType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::NewProp_Cout = { "Cout", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnAmmoChange_Parms, Cout), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::NewProp_TypeAmmo_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::NewProp_TypeAmmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::NewProp_Cout,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MyProject, nullptr, "OnAmmoChange__DelegateSignature", nullptr, nullptr, sizeof(_Script_MyProject_eventOnAmmoChange_Parms), Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics
	{
		struct _Script_MyProject_eventOnSwitchWeapon_Parms
		{
			FName WeaponIdName;
			FAdditionalWeaponInfo WeaponAdditionalInfo;
			int32 NewCurrentIndexWeapon;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_WeaponIdName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WeaponAdditionalInfo;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NewCurrentIndexWeapon;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::NewProp_WeaponIdName = { "WeaponIdName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnSwitchWeapon_Parms, WeaponIdName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::NewProp_WeaponAdditionalInfo = { "WeaponAdditionalInfo", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnSwitchWeapon_Parms, WeaponAdditionalInfo), Z_Construct_UScriptStruct_FAdditionalWeaponInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::NewProp_NewCurrentIndexWeapon = { "NewCurrentIndexWeapon", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnSwitchWeapon_Parms, NewCurrentIndexWeapon), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::NewProp_WeaponIdName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::NewProp_WeaponAdditionalInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::NewProp_NewCurrentIndexWeapon,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MyProject, nullptr, "OnSwitchWeapon__DelegateSignature", nullptr, nullptr, sizeof(_Script_MyProject_eventOnSwitchWeapon_Parms), Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MyProject_OnSwitchWeapon__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UTPSInventoryComponent::execGetDropItemInfoFromInventory)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_IndexSlot);
		P_GET_STRUCT_REF(FDropItem,Z_Param_Out_DropItemInfo);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetDropItemInfoFromInventory(Z_Param_IndexSlot,Z_Param_Out_DropItemInfo);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTPSInventoryComponent::execTryGetWeaponToInventory)
	{
		P_GET_STRUCT(FWeaponSlot,Z_Param_NewWeapon);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryGetWeaponToInventory(Z_Param_NewWeapon);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTPSInventoryComponent::execSwitchWeaponToInventory)
	{
		P_GET_STRUCT(FWeaponSlot,Z_Param_NewWeapon);
		P_GET_PROPERTY(FIntProperty,Z_Param_IndexSlot);
		P_GET_PROPERTY(FIntProperty,Z_Param_CurrentIndexWeaponChar);
		P_GET_STRUCT_REF(FDropItem,Z_Param_Out_DropItemInfo);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SwitchWeaponToInventory(Z_Param_NewWeapon,Z_Param_IndexSlot,Z_Param_CurrentIndexWeaponChar,Z_Param_Out_DropItemInfo);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTPSInventoryComponent::execCheckCanTakeWeapon)
	{
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_FreeSlot);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CheckCanTakeWeapon(Z_Param_Out_FreeSlot);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTPSInventoryComponent::execCheckCanTakeAmmo)
	{
		P_GET_ENUM(EWeaponType,Z_Param_AmmoType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CheckCanTakeAmmo(EWeaponType(Z_Param_AmmoType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTPSInventoryComponent::execAmmoSlotChangeValue)
	{
		P_GET_ENUM(EWeaponType,Z_Param_TypeWeapon);
		P_GET_PROPERTY(FIntProperty,Z_Param_CoutChangeAmmo);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AmmoSlotChangeValue(EWeaponType(Z_Param_TypeWeapon),Z_Param_CoutChangeAmmo);
		P_NATIVE_END;
	}
	void UTPSInventoryComponent::StaticRegisterNativesUTPSInventoryComponent()
	{
		UClass* Class = UTPSInventoryComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AmmoSlotChangeValue", &UTPSInventoryComponent::execAmmoSlotChangeValue },
			{ "CheckCanTakeAmmo", &UTPSInventoryComponent::execCheckCanTakeAmmo },
			{ "CheckCanTakeWeapon", &UTPSInventoryComponent::execCheckCanTakeWeapon },
			{ "GetDropItemInfoFromInventory", &UTPSInventoryComponent::execGetDropItemInfoFromInventory },
			{ "SwitchWeaponToInventory", &UTPSInventoryComponent::execSwitchWeaponToInventory },
			{ "TryGetWeaponToInventory", &UTPSInventoryComponent::execTryGetWeaponToInventory },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics
	{
		struct TPSInventoryComponent_eventAmmoSlotChangeValue_Parms
		{
			EWeaponType TypeWeapon;
			int32 CoutChangeAmmo;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TypeWeapon_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TypeWeapon;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CoutChangeAmmo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::NewProp_TypeWeapon_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::NewProp_TypeWeapon = { "TypeWeapon", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TPSInventoryComponent_eventAmmoSlotChangeValue_Parms, TypeWeapon), Z_Construct_UEnum_MyProject_EWeaponType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::NewProp_CoutChangeAmmo = { "CoutChangeAmmo", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TPSInventoryComponent_eventAmmoSlotChangeValue_Parms, CoutChangeAmmo), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::NewProp_TypeWeapon_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::NewProp_TypeWeapon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::NewProp_CoutChangeAmmo,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTPSInventoryComponent, nullptr, "AmmoSlotChangeValue", nullptr, nullptr, sizeof(TPSInventoryComponent_eventAmmoSlotChangeValue_Parms), Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics
	{
		struct TPSInventoryComponent_eventCheckCanTakeAmmo_Parms
		{
			EWeaponType AmmoType;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AmmoType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AmmoType;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::NewProp_AmmoType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::NewProp_AmmoType = { "AmmoType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TPSInventoryComponent_eventCheckCanTakeAmmo_Parms, AmmoType), Z_Construct_UEnum_MyProject_EWeaponType, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TPSInventoryComponent_eventCheckCanTakeAmmo_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TPSInventoryComponent_eventCheckCanTakeAmmo_Parms), &Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::NewProp_AmmoType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::NewProp_AmmoType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::Function_MetaDataParams[] = {
		{ "Category", "Interface" },
		{ "Comment", "//Interface PickUp Actors\n" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
		{ "ToolTip", "Interface PickUp Actors" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTPSInventoryComponent, nullptr, "CheckCanTakeAmmo", nullptr, nullptr, sizeof(TPSInventoryComponent_eventCheckCanTakeAmmo_Parms), Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics
	{
		struct TPSInventoryComponent_eventCheckCanTakeWeapon_Parms
		{
			int32 FreeSlot;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FreeSlot;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::NewProp_FreeSlot = { "FreeSlot", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TPSInventoryComponent_eventCheckCanTakeWeapon_Parms, FreeSlot), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TPSInventoryComponent_eventCheckCanTakeWeapon_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TPSInventoryComponent_eventCheckCanTakeWeapon_Parms), &Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::NewProp_FreeSlot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::Function_MetaDataParams[] = {
		{ "Category", "Interface" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTPSInventoryComponent, nullptr, "CheckCanTakeWeapon", nullptr, nullptr, sizeof(TPSInventoryComponent_eventCheckCanTakeWeapon_Parms), Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics
	{
		struct TPSInventoryComponent_eventGetDropItemInfoFromInventory_Parms
		{
			int32 IndexSlot;
			FDropItem DropItemInfo;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IndexSlot;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DropItemInfo;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::NewProp_IndexSlot = { "IndexSlot", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TPSInventoryComponent_eventGetDropItemInfoFromInventory_Parms, IndexSlot), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::NewProp_DropItemInfo = { "DropItemInfo", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TPSInventoryComponent_eventGetDropItemInfoFromInventory_Parms, DropItemInfo), Z_Construct_UScriptStruct_FDropItem, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TPSInventoryComponent_eventGetDropItemInfoFromInventory_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TPSInventoryComponent_eventGetDropItemInfoFromInventory_Parms), &Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::NewProp_IndexSlot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::NewProp_DropItemInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::Function_MetaDataParams[] = {
		{ "Category", "Interface" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTPSInventoryComponent, nullptr, "GetDropItemInfoFromInventory", nullptr, nullptr, sizeof(TPSInventoryComponent_eventGetDropItemInfoFromInventory_Parms), Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics
	{
		struct TPSInventoryComponent_eventSwitchWeaponToInventory_Parms
		{
			FWeaponSlot NewWeapon;
			int32 IndexSlot;
			int32 CurrentIndexWeaponChar;
			FDropItem DropItemInfo;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewWeapon;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IndexSlot;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CurrentIndexWeaponChar;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DropItemInfo;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::NewProp_NewWeapon = { "NewWeapon", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TPSInventoryComponent_eventSwitchWeaponToInventory_Parms, NewWeapon), Z_Construct_UScriptStruct_FWeaponSlot, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::NewProp_IndexSlot = { "IndexSlot", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TPSInventoryComponent_eventSwitchWeaponToInventory_Parms, IndexSlot), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::NewProp_CurrentIndexWeaponChar = { "CurrentIndexWeaponChar", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TPSInventoryComponent_eventSwitchWeaponToInventory_Parms, CurrentIndexWeaponChar), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::NewProp_DropItemInfo = { "DropItemInfo", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TPSInventoryComponent_eventSwitchWeaponToInventory_Parms, DropItemInfo), Z_Construct_UScriptStruct_FDropItem, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TPSInventoryComponent_eventSwitchWeaponToInventory_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TPSInventoryComponent_eventSwitchWeaponToInventory_Parms), &Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::NewProp_NewWeapon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::NewProp_IndexSlot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::NewProp_CurrentIndexWeaponChar,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::NewProp_DropItemInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::Function_MetaDataParams[] = {
		{ "Category", "Interface" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTPSInventoryComponent, nullptr, "SwitchWeaponToInventory", nullptr, nullptr, sizeof(TPSInventoryComponent_eventSwitchWeaponToInventory_Parms), Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics
	{
		struct TPSInventoryComponent_eventTryGetWeaponToInventory_Parms
		{
			FWeaponSlot NewWeapon;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewWeapon;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::NewProp_NewWeapon = { "NewWeapon", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TPSInventoryComponent_eventTryGetWeaponToInventory_Parms, NewWeapon), Z_Construct_UScriptStruct_FWeaponSlot, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TPSInventoryComponent_eventTryGetWeaponToInventory_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TPSInventoryComponent_eventTryGetWeaponToInventory_Parms), &Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::NewProp_NewWeapon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::Function_MetaDataParams[] = {
		{ "Category", "Interface" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTPSInventoryComponent, nullptr, "TryGetWeaponToInventory", nullptr, nullptr, sizeof(TPSInventoryComponent_eventTryGetWeaponToInventory_Parms), Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UTPSInventoryComponent_NoRegister()
	{
		return UTPSInventoryComponent::StaticClass();
	}
	struct Z_Construct_UClass_UTPSInventoryComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnAmmoChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnAmmoChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnWeaponAdditionalInfoChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnWeaponAdditionalInfoChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnWeaponAmmoEmpty_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnWeaponAmmoEmpty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnWeaponAmmoAviable_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnWeaponAmmoAviable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnUpdateWeaponSlots_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnUpdateWeaponSlots;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WeaponSlots_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeaponSlots_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_WeaponSlots;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AmmoSlots_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AmmoSlots_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AmmoSlots;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTPSInventoryComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MyProject,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTPSInventoryComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTPSInventoryComponent_AmmoSlotChangeValue, "AmmoSlotChangeValue" }, // 2256951752
		{ &Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeAmmo, "CheckCanTakeAmmo" }, // 2255030722
		{ &Z_Construct_UFunction_UTPSInventoryComponent_CheckCanTakeWeapon, "CheckCanTakeWeapon" }, // 1304054720
		{ &Z_Construct_UFunction_UTPSInventoryComponent_GetDropItemInfoFromInventory, "GetDropItemInfoFromInventory" }, // 3671722492
		{ &Z_Construct_UFunction_UTPSInventoryComponent_SwitchWeaponToInventory, "SwitchWeaponToInventory" }, // 3824060440
		{ &Z_Construct_UFunction_UTPSInventoryComponent_TryGetWeaponToInventory, "TryGetWeaponToInventory" }, // 3966805149
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTPSInventoryComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "TPSInventoryComponent.h" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnAmmoChange_MetaData[] = {
		{ "Category", "Inventory" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnAmmoChange = { "OnAmmoChange", nullptr, (EPropertyFlags)0x0010000010080005, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTPSInventoryComponent, OnAmmoChange), Z_Construct_UDelegateFunction_MyProject_OnAmmoChange__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnAmmoChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnAmmoChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAdditionalInfoChange_MetaData[] = {
		{ "Category", "Inventory" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAdditionalInfoChange = { "OnWeaponAdditionalInfoChange", nullptr, (EPropertyFlags)0x0010000010080005, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTPSInventoryComponent, OnWeaponAdditionalInfoChange), Z_Construct_UDelegateFunction_MyProject_OnWeaponAdditionalInfoChange__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAdditionalInfoChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAdditionalInfoChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAmmoEmpty_MetaData[] = {
		{ "Category", "Inventory" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAmmoEmpty = { "OnWeaponAmmoEmpty", nullptr, (EPropertyFlags)0x0010000010080005, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTPSInventoryComponent, OnWeaponAmmoEmpty), Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoEmpty__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAmmoEmpty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAmmoEmpty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAmmoAviable_MetaData[] = {
		{ "Category", "Inventory" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAmmoAviable = { "OnWeaponAmmoAviable", nullptr, (EPropertyFlags)0x0010000010080005, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTPSInventoryComponent, OnWeaponAmmoAviable), Z_Construct_UDelegateFunction_MyProject_OnWeaponAmmoAviable__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAmmoAviable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAmmoAviable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnUpdateWeaponSlots_MetaData[] = {
		{ "Category", "Inventory" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnUpdateWeaponSlots = { "OnUpdateWeaponSlots", nullptr, (EPropertyFlags)0x0010000010080005, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTPSInventoryComponent, OnUpdateWeaponSlots), Z_Construct_UDelegateFunction_MyProject_OnUpdateWeaponSlots__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnUpdateWeaponSlots_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnUpdateWeaponSlots_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_WeaponSlots_Inner = { "WeaponSlots", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FWeaponSlot, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_WeaponSlots_MetaData[] = {
		{ "Category", "Weapons" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_WeaponSlots = { "WeaponSlots", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTPSInventoryComponent, WeaponSlots), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_WeaponSlots_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_WeaponSlots_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_AmmoSlots_Inner = { "AmmoSlots", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FAmmoSlot, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_AmmoSlots_MetaData[] = {
		{ "Category", "Weapons" },
		{ "ModuleRelativePath", "TPSInventoryComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_AmmoSlots = { "AmmoSlots", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTPSInventoryComponent, AmmoSlots), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_AmmoSlots_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_AmmoSlots_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTPSInventoryComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnAmmoChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAdditionalInfoChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAmmoEmpty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnWeaponAmmoAviable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_OnUpdateWeaponSlots,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_WeaponSlots_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_WeaponSlots,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_AmmoSlots_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTPSInventoryComponent_Statics::NewProp_AmmoSlots,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTPSInventoryComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTPSInventoryComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTPSInventoryComponent_Statics::ClassParams = {
		&UTPSInventoryComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UTPSInventoryComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UTPSInventoryComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTPSInventoryComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTPSInventoryComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTPSInventoryComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTPSInventoryComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTPSInventoryComponent, 4135080742);
	template<> MYPROJECT_API UClass* StaticClass<UTPSInventoryComponent>()
	{
		return UTPSInventoryComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTPSInventoryComponent(Z_Construct_UClass_UTPSInventoryComponent, &UTPSInventoryComponent::StaticClass, TEXT("/Script/MyProject"), TEXT("UTPSInventoryComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTPSInventoryComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
