// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT_Types_generated_h
#error "Types.generated.h already included, missing '#pragma once' in Types.h"
#endif
#define MYPROJECT_Types_generated_h

#define MyProject_Source_MyProject_Types_h_290_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDropItem_Statics; \
	MYPROJECT_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> MYPROJECT_API UScriptStruct* StaticStruct<struct FDropItem>();

#define MyProject_Source_MyProject_Types_h_276_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAmmoSlot_Statics; \
	MYPROJECT_API static class UScriptStruct* StaticStruct();


template<> MYPROJECT_API UScriptStruct* StaticStruct<struct FAmmoSlot>();

#define MyProject_Source_MyProject_Types_h_266_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FWeaponSlot_Statics; \
	MYPROJECT_API static class UScriptStruct* StaticStruct();


template<> MYPROJECT_API UScriptStruct* StaticStruct<struct FWeaponSlot>();

#define MyProject_Source_MyProject_Types_h_257_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAdditionalWeaponInfo_Statics; \
	MYPROJECT_API static class UScriptStruct* StaticStruct();


template<> MYPROJECT_API UScriptStruct* StaticStruct<struct FAdditionalWeaponInfo>();

#define MyProject_Source_MyProject_Types_h_190_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FWeaponInfo_Statics; \
	MYPROJECT_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> MYPROJECT_API UScriptStruct* StaticStruct<struct FWeaponInfo>();

#define MyProject_Source_MyProject_Types_h_144_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FWeaponDispersion_Statics; \
	MYPROJECT_API static class UScriptStruct* StaticStruct();


template<> MYPROJECT_API UScriptStruct* StaticStruct<struct FWeaponDispersion>();

#define MyProject_Source_MyProject_Types_h_97_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FProjectileInfo_Statics; \
	MYPROJECT_API static class UScriptStruct* StaticStruct();


template<> MYPROJECT_API UScriptStruct* StaticStruct<struct FProjectileInfo>();

#define MyProject_Source_MyProject_Types_h_73_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDropMeshInfo_Statics; \
	MYPROJECT_API static class UScriptStruct* StaticStruct();


template<> MYPROJECT_API UScriptStruct* StaticStruct<struct FDropMeshInfo>();

#define MyProject_Source_MyProject_Types_h_50_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAnimationWeaponInfo_Statics; \
	MYPROJECT_API static class UScriptStruct* StaticStruct();


template<> MYPROJECT_API UScriptStruct* StaticStruct<struct FAnimationWeaponInfo>();

#define MyProject_Source_MyProject_Types_h_31_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCharacterSpeed_Statics; \
	MYPROJECT_API static class UScriptStruct* StaticStruct();


template<> MYPROJECT_API UScriptStruct* StaticStruct<struct FCharacterSpeed>();

#define MyProject_Source_MyProject_Types_h_309_SPARSE_DATA
#define MyProject_Source_MyProject_Types_h_309_RPC_WRAPPERS
#define MyProject_Source_MyProject_Types_h_309_RPC_WRAPPERS_NO_PURE_DECLS
#define MyProject_Source_MyProject_Types_h_309_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTypes(); \
	friend struct Z_Construct_UClass_UTypes_Statics; \
public: \
	DECLARE_CLASS(UTypes, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(UTypes)


#define MyProject_Source_MyProject_Types_h_309_INCLASS \
private: \
	static void StaticRegisterNativesUTypes(); \
	friend struct Z_Construct_UClass_UTypes_Statics; \
public: \
	DECLARE_CLASS(UTypes, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(UTypes)


#define MyProject_Source_MyProject_Types_h_309_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTypes) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTypes); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTypes(UTypes&&); \
	NO_API UTypes(const UTypes&); \
public:


#define MyProject_Source_MyProject_Types_h_309_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTypes(UTypes&&); \
	NO_API UTypes(const UTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTypes)


#define MyProject_Source_MyProject_Types_h_309_PRIVATE_PROPERTY_OFFSET
#define MyProject_Source_MyProject_Types_h_306_PROLOG
#define MyProject_Source_MyProject_Types_h_309_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_Types_h_309_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_Types_h_309_SPARSE_DATA \
	MyProject_Source_MyProject_Types_h_309_RPC_WRAPPERS \
	MyProject_Source_MyProject_Types_h_309_INCLASS \
	MyProject_Source_MyProject_Types_h_309_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject_Source_MyProject_Types_h_309_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_Types_h_309_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_Types_h_309_SPARSE_DATA \
	MyProject_Source_MyProject_Types_h_309_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject_Source_MyProject_Types_h_309_INCLASS_NO_PURE_DECLS \
	MyProject_Source_MyProject_Types_h_309_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class UTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject_Source_MyProject_Types_h


#define FOREACH_ENUM_EWEAPONTYPE(op) \
	op(EWeaponType::RifleType) \
	op(EWeaponType::ShotGunType) \
	op(EWeaponType::SniperRifle) \
	op(EWeaponType::GrenadeLauncher) \
	op(EWeaponType::RocketLauncher) 

enum class EWeaponType : uint8;
template<> MYPROJECT_API UEnum* StaticEnum<EWeaponType>();

#define FOREACH_ENUM_EMOVEMENTSTATE(op) \
	op(EMovementState::Aim_State) \
	op(EMovementState::AimWalk_State) \
	op(EMovementState::Walk_State) \
	op(EMovementState::Run_State) \
	op(EMovementState::SprintRun_State) 

enum class EMovementState : uint8;
template<> MYPROJECT_API UEnum* StaticEnum<EMovementState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
