// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MyProject/WeaponDefault.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWeaponDefault() {}
// Cross Module References
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MyProject();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature();
	ENGINE_API UClass* Z_Construct_UClass_UAnimMontage_NoRegister();
	MYPROJECT_API UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature();
	MYPROJECT_API UClass* Z_Construct_UClass_AWeaponDefault_NoRegister();
	MYPROJECT_API UClass* Z_Construct_UClass_AWeaponDefault();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UArrowComponent_NoRegister();
	MYPROJECT_API UScriptStruct* Z_Construct_UScriptStruct_FWeaponInfo();
	MYPROJECT_API UScriptStruct* Z_Construct_UScriptStruct_FAdditionalWeaponInfo();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics
	{
		struct _Script_MyProject_eventOnWeaponReloadEnd_Parms
		{
			bool bIsSuccess;
			int32 AmmoSafe;
		};
		static void NewProp_bIsSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSuccess;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AmmoSafe;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::NewProp_bIsSuccess_SetBit(void* Obj)
	{
		((_Script_MyProject_eventOnWeaponReloadEnd_Parms*)Obj)->bIsSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::NewProp_bIsSuccess = { "bIsSuccess", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MyProject_eventOnWeaponReloadEnd_Parms), &Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::NewProp_bIsSuccess_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::NewProp_AmmoSafe = { "AmmoSafe", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnWeaponReloadEnd_Parms, AmmoSafe), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::NewProp_bIsSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::NewProp_AmmoSafe,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MyProject, nullptr, "OnWeaponReloadEnd__DelegateSignature", nullptr, nullptr, sizeof(_Script_MyProject_eventOnWeaponReloadEnd_Parms), Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadEnd__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature_Statics
	{
		struct _Script_MyProject_eventOnWeaponReloadStart_Parms
		{
			UAnimMontage* Anim;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Anim;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature_Statics::NewProp_Anim = { "Anim", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnWeaponReloadStart_Parms, Anim), Z_Construct_UClass_UAnimMontage_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature_Statics::NewProp_Anim,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MyProject, nullptr, "OnWeaponReloadStart__DelegateSignature", nullptr, nullptr, sizeof(_Script_MyProject_eventOnWeaponReloadStart_Parms), Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MyProject_OnWeaponReloadStart__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature_Statics
	{
		struct _Script_MyProject_eventOnWeaponFireStart_Parms
		{
			UAnimMontage* AnimFireChar;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AnimFireChar;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature_Statics::NewProp_AnimFireChar = { "AnimFireChar", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MyProject_eventOnWeaponFireStart_Parms, AnimFireChar), Z_Construct_UClass_UAnimMontage_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature_Statics::NewProp_AnimFireChar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponFireStart);//ToDo Delegate on event weapon fire - Anim char, state char...\n" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
		{ "ToolTip", "DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponFireStart);ToDo Delegate on event weapon fire - Anim char, state char..." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MyProject, nullptr, "OnWeaponFireStart__DelegateSignature", nullptr, nullptr, sizeof(_Script_MyProject_eventOnWeaponFireStart_Parms), Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MyProject_OnWeaponFireStart__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(AWeaponDefault::execInitDropMesh)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_DropMesh);
		P_GET_STRUCT(FTransform,Z_Param_Offset);
		P_GET_STRUCT(FVector,Z_Param_DropImpulseDirection);
		P_GET_PROPERTY(FFloatProperty,Z_Param_LifeTimeMesh);
		P_GET_PROPERTY(FFloatProperty,Z_Param_ImpulseRandomDispersion);
		P_GET_PROPERTY(FFloatProperty,Z_Param_PowerImpulse);
		P_GET_PROPERTY(FFloatProperty,Z_Param_CustomMass);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InitDropMesh(Z_Param_DropMesh,Z_Param_Offset,Z_Param_DropImpulseDirection,Z_Param_LifeTimeMesh,Z_Param_ImpulseRandomDispersion,Z_Param_PowerImpulse,Z_Param_CustomMass);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWeaponDefault::execInitReload)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InitReload();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWeaponDefault::execGetWeaponRound)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetWeaponRound();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWeaponDefault::execFire)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Fire();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWeaponDefault::execSetWeaponStateFire)
	{
		P_GET_UBOOL(Z_Param_bIsFire);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetWeaponStateFire(Z_Param_bIsFire);
		P_NATIVE_END;
	}
	void AWeaponDefault::StaticRegisterNativesAWeaponDefault()
	{
		UClass* Class = AWeaponDefault::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Fire", &AWeaponDefault::execFire },
			{ "GetWeaponRound", &AWeaponDefault::execGetWeaponRound },
			{ "InitDropMesh", &AWeaponDefault::execInitDropMesh },
			{ "InitReload", &AWeaponDefault::execInitReload },
			{ "SetWeaponStateFire", &AWeaponDefault::execSetWeaponStateFire },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AWeaponDefault_Fire_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWeaponDefault_Fire_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWeaponDefault_Fire_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWeaponDefault, nullptr, "Fire", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWeaponDefault_Fire_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWeaponDefault_Fire_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWeaponDefault_Fire()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWeaponDefault_Fire_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWeaponDefault_GetWeaponRound_Statics
	{
		struct WeaponDefault_eventGetWeaponRound_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AWeaponDefault_GetWeaponRound_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponDefault_eventGetWeaponRound_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWeaponDefault_GetWeaponRound_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWeaponDefault_GetWeaponRound_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWeaponDefault_GetWeaponRound_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWeaponDefault_GetWeaponRound_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWeaponDefault, nullptr, "GetWeaponRound", nullptr, nullptr, sizeof(WeaponDefault_eventGetWeaponRound_Parms), Z_Construct_UFunction_AWeaponDefault_GetWeaponRound_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWeaponDefault_GetWeaponRound_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWeaponDefault_GetWeaponRound_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWeaponDefault_GetWeaponRound_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWeaponDefault_GetWeaponRound()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWeaponDefault_GetWeaponRound_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics
	{
		struct WeaponDefault_eventInitDropMesh_Parms
		{
			UStaticMesh* DropMesh;
			FTransform Offset;
			FVector DropImpulseDirection;
			float LifeTimeMesh;
			float ImpulseRandomDispersion;
			float PowerImpulse;
			float CustomMass;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DropMesh;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Offset;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DropImpulseDirection;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LifeTimeMesh;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ImpulseRandomDispersion;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PowerImpulse;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CustomMass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_DropMesh = { "DropMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponDefault_eventInitDropMesh_Parms, DropMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_Offset = { "Offset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponDefault_eventInitDropMesh_Parms, Offset), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_DropImpulseDirection = { "DropImpulseDirection", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponDefault_eventInitDropMesh_Parms, DropImpulseDirection), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_LifeTimeMesh = { "LifeTimeMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponDefault_eventInitDropMesh_Parms, LifeTimeMesh), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_ImpulseRandomDispersion = { "ImpulseRandomDispersion", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponDefault_eventInitDropMesh_Parms, ImpulseRandomDispersion), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_PowerImpulse = { "PowerImpulse", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponDefault_eventInitDropMesh_Parms, PowerImpulse), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_CustomMass = { "CustomMass", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponDefault_eventInitDropMesh_Parms, CustomMass), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_DropMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_Offset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_DropImpulseDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_LifeTimeMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_ImpulseRandomDispersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_PowerImpulse,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::NewProp_CustomMass,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWeaponDefault, nullptr, "InitDropMesh", nullptr, nullptr, sizeof(WeaponDefault_eventInitDropMesh_Parms), Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWeaponDefault_InitDropMesh()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWeaponDefault_InitDropMesh_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWeaponDefault_InitReload_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWeaponDefault_InitReload_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWeaponDefault_InitReload_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWeaponDefault, nullptr, "InitReload", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWeaponDefault_InitReload_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWeaponDefault_InitReload_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWeaponDefault_InitReload()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWeaponDefault_InitReload_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics
	{
		struct WeaponDefault_eventSetWeaponStateFire_Parms
		{
			bool bIsFire;
		};
		static void NewProp_bIsFire_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsFire;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics::NewProp_bIsFire_SetBit(void* Obj)
	{
		((WeaponDefault_eventSetWeaponStateFire_Parms*)Obj)->bIsFire = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics::NewProp_bIsFire = { "bIsFire", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WeaponDefault_eventSetWeaponStateFire_Parms), &Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics::NewProp_bIsFire_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics::NewProp_bIsFire,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWeaponDefault, nullptr, "SetWeaponStateFire", nullptr, nullptr, sizeof(WeaponDefault_eventSetWeaponStateFire_Parms), Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AWeaponDefault_NoRegister()
	{
		return AWeaponDefault::StaticClass();
	}
	struct Z_Construct_UClass_AWeaponDefault_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletalMeshWeapon_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMeshWeapon;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshWeapon_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshWeapon;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShootLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShootLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeaponSetting_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WeaponSetting;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdditionalWeaponInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdditionalWeaponInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReloadTimer_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReloadTimer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReloadTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReloadTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeaponFiring_MetaData[];
#endif
		static void NewProp_WeaponFiring_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_WeaponFiring;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeaponReloading_MetaData[];
#endif
		static void NewProp_WeaponReloading_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_WeaponReloading;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShowDebug_MetaData[];
#endif
		static void NewProp_ShowDebug_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ShowDebug;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SizeVectorToChangeShootDirectionLogic_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SizeVectorToChangeShootDirectionLogic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWeaponDefault_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MyProject,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AWeaponDefault_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AWeaponDefault_Fire, "Fire" }, // 387132458
		{ &Z_Construct_UFunction_AWeaponDefault_GetWeaponRound, "GetWeaponRound" }, // 2814796414
		{ &Z_Construct_UFunction_AWeaponDefault_InitDropMesh, "InitDropMesh" }, // 2805584163
		{ &Z_Construct_UFunction_AWeaponDefault_InitReload, "InitReload" }, // 2901761239
		{ &Z_Construct_UFunction_AWeaponDefault_SetWeaponStateFire, "SetWeaponStateFire" }, // 3243338262
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WeaponDefault.h" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SceneComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SceneComponent = { "SceneComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponDefault, SceneComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SceneComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SceneComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SkeletalMeshWeapon_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SkeletalMeshWeapon = { "SkeletalMeshWeapon", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponDefault, SkeletalMeshWeapon), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SkeletalMeshWeapon_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SkeletalMeshWeapon_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::NewProp_StaticMeshWeapon_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWeaponDefault_Statics::NewProp_StaticMeshWeapon = { "StaticMeshWeapon", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponDefault, StaticMeshWeapon), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_StaticMeshWeapon_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_StaticMeshWeapon_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ShootLocation_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ShootLocation = { "ShootLocation", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponDefault, ShootLocation), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ShootLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ShootLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponSetting_MetaData[] = {
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponSetting = { "WeaponSetting", nullptr, (EPropertyFlags)0x0010008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponDefault, WeaponSetting), Z_Construct_UScriptStruct_FWeaponInfo, METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponSetting_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponSetting_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::NewProp_AdditionalWeaponInfo_MetaData[] = {
		{ "Category", "Weapon Info" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWeaponDefault_Statics::NewProp_AdditionalWeaponInfo = { "AdditionalWeaponInfo", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponDefault, AdditionalWeaponInfo), Z_Construct_UScriptStruct_FAdditionalWeaponInfo, METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_AdditionalWeaponInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_AdditionalWeaponInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ReloadTimer_MetaData[] = {
		{ "Category", "ReloadLogic" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ReloadTimer = { "ReloadTimer", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponDefault, ReloadTimer), METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ReloadTimer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ReloadTimer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ReloadTime_MetaData[] = {
		{ "Category", "ReloadLogic Debug" },
		{ "Comment", "//Remove !!! Debug\n" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
		{ "ToolTip", "Remove !!! Debug" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ReloadTime = { "ReloadTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponDefault, ReloadTime), METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ReloadTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ReloadTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponFiring_MetaData[] = {
		{ "Category", "FireLogic" },
		{ "Comment", "//flags\n" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
		{ "ToolTip", "flags" },
	};
#endif
	void Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponFiring_SetBit(void* Obj)
	{
		((AWeaponDefault*)Obj)->WeaponFiring = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponFiring = { "WeaponFiring", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWeaponDefault), &Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponFiring_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponFiring_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponFiring_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponReloading_MetaData[] = {
		{ "Category", "ReloadLogic" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	void Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponReloading_SetBit(void* Obj)
	{
		((AWeaponDefault*)Obj)->WeaponReloading = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponReloading = { "WeaponReloading", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWeaponDefault), &Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponReloading_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponReloading_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponReloading_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ShowDebug_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	void Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ShowDebug_SetBit(void* Obj)
	{
		((AWeaponDefault*)Obj)->ShowDebug = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ShowDebug = { "ShowDebug", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWeaponDefault), &Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ShowDebug_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ShowDebug_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ShowDebug_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SizeVectorToChangeShootDirectionLogic_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "WeaponDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SizeVectorToChangeShootDirectionLogic = { "SizeVectorToChangeShootDirectionLogic", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponDefault, SizeVectorToChangeShootDirectionLogic), METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SizeVectorToChangeShootDirectionLogic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SizeVectorToChangeShootDirectionLogic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWeaponDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SceneComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SkeletalMeshWeapon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponDefault_Statics::NewProp_StaticMeshWeapon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ShootLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponSetting,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponDefault_Statics::NewProp_AdditionalWeaponInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ReloadTimer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ReloadTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponFiring,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponDefault_Statics::NewProp_WeaponReloading,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponDefault_Statics::NewProp_ShowDebug,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponDefault_Statics::NewProp_SizeVectorToChangeShootDirectionLogic,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWeaponDefault_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWeaponDefault>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWeaponDefault_Statics::ClassParams = {
		&AWeaponDefault::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AWeaponDefault_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWeaponDefault_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponDefault_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWeaponDefault()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWeaponDefault_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWeaponDefault, 527332125);
	template<> MYPROJECT_API UClass* StaticClass<AWeaponDefault>()
	{
		return AWeaponDefault::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWeaponDefault(Z_Construct_UClass_AWeaponDefault, &AWeaponDefault::StaticClass, TEXT("/Script/MyProject"), TEXT("AWeaponDefault"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWeaponDefault);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
