// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UAnimMontage;
class UStaticMesh;
struct FTransform;
struct FVector;
#ifdef MYPROJECT_WeaponDefault_generated_h
#error "WeaponDefault.generated.h already included, missing '#pragma once' in WeaponDefault.h"
#endif
#define MYPROJECT_WeaponDefault_generated_h

#define MyProject_Source_MyProject_WeaponDefault_h_17_DELEGATE \
struct _Script_MyProject_eventOnWeaponReloadEnd_Parms \
{ \
	bool bIsSuccess; \
	int32 AmmoSafe; \
}; \
static inline void FOnWeaponReloadEnd_DelegateWrapper(const FMulticastScriptDelegate& OnWeaponReloadEnd, bool bIsSuccess, int32 AmmoSafe) \
{ \
	_Script_MyProject_eventOnWeaponReloadEnd_Parms Parms; \
	Parms.bIsSuccess=bIsSuccess ? true : false; \
	Parms.AmmoSafe=AmmoSafe; \
	OnWeaponReloadEnd.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define MyProject_Source_MyProject_WeaponDefault_h_16_DELEGATE \
struct _Script_MyProject_eventOnWeaponReloadStart_Parms \
{ \
	UAnimMontage* Anim; \
}; \
static inline void FOnWeaponReloadStart_DelegateWrapper(const FMulticastScriptDelegate& OnWeaponReloadStart, UAnimMontage* Anim) \
{ \
	_Script_MyProject_eventOnWeaponReloadStart_Parms Parms; \
	Parms.Anim=Anim; \
	OnWeaponReloadStart.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define MyProject_Source_MyProject_WeaponDefault_h_15_DELEGATE \
struct _Script_MyProject_eventOnWeaponFireStart_Parms \
{ \
	UAnimMontage* AnimFireChar; \
}; \
static inline void FOnWeaponFireStart_DelegateWrapper(const FMulticastScriptDelegate& OnWeaponFireStart, UAnimMontage* AnimFireChar) \
{ \
	_Script_MyProject_eventOnWeaponFireStart_Parms Parms; \
	Parms.AnimFireChar=AnimFireChar; \
	OnWeaponFireStart.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define MyProject_Source_MyProject_WeaponDefault_h_24_SPARSE_DATA
#define MyProject_Source_MyProject_WeaponDefault_h_24_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInitDropMesh); \
	DECLARE_FUNCTION(execInitReload); \
	DECLARE_FUNCTION(execGetWeaponRound); \
	DECLARE_FUNCTION(execFire); \
	DECLARE_FUNCTION(execSetWeaponStateFire);


#define MyProject_Source_MyProject_WeaponDefault_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInitDropMesh); \
	DECLARE_FUNCTION(execInitReload); \
	DECLARE_FUNCTION(execGetWeaponRound); \
	DECLARE_FUNCTION(execFire); \
	DECLARE_FUNCTION(execSetWeaponStateFire);


#define MyProject_Source_MyProject_WeaponDefault_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWeaponDefault(); \
	friend struct Z_Construct_UClass_AWeaponDefault_Statics; \
public: \
	DECLARE_CLASS(AWeaponDefault, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(AWeaponDefault)


#define MyProject_Source_MyProject_WeaponDefault_h_24_INCLASS \
private: \
	static void StaticRegisterNativesAWeaponDefault(); \
	friend struct Z_Construct_UClass_AWeaponDefault_Statics; \
public: \
	DECLARE_CLASS(AWeaponDefault, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(AWeaponDefault)


#define MyProject_Source_MyProject_WeaponDefault_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWeaponDefault(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWeaponDefault) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponDefault); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponDefault); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponDefault(AWeaponDefault&&); \
	NO_API AWeaponDefault(const AWeaponDefault&); \
public:


#define MyProject_Source_MyProject_WeaponDefault_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponDefault(AWeaponDefault&&); \
	NO_API AWeaponDefault(const AWeaponDefault&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponDefault); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponDefault); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWeaponDefault)


#define MyProject_Source_MyProject_WeaponDefault_h_24_PRIVATE_PROPERTY_OFFSET
#define MyProject_Source_MyProject_WeaponDefault_h_21_PROLOG
#define MyProject_Source_MyProject_WeaponDefault_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_WeaponDefault_h_24_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_WeaponDefault_h_24_SPARSE_DATA \
	MyProject_Source_MyProject_WeaponDefault_h_24_RPC_WRAPPERS \
	MyProject_Source_MyProject_WeaponDefault_h_24_INCLASS \
	MyProject_Source_MyProject_WeaponDefault_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject_Source_MyProject_WeaponDefault_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_WeaponDefault_h_24_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_WeaponDefault_h_24_SPARSE_DATA \
	MyProject_Source_MyProject_WeaponDefault_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject_Source_MyProject_WeaponDefault_h_24_INCLASS_NO_PURE_DECLS \
	MyProject_Source_MyProject_WeaponDefault_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class AWeaponDefault>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject_Source_MyProject_WeaponDefault_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
