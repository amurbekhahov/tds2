// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MyProject/WorldItemDefault.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWorldItemDefault() {}
// Cross Module References
	MYPROJECT_API UClass* Z_Construct_UClass_AWorldItemDefault_NoRegister();
	MYPROJECT_API UClass* Z_Construct_UClass_AWorldItemDefault();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_MyProject();
// End Cross Module References
	void AWorldItemDefault::StaticRegisterNativesAWorldItemDefault()
	{
	}
	UClass* Z_Construct_UClass_AWorldItemDefault_NoRegister()
	{
		return AWorldItemDefault::StaticClass();
	}
	struct Z_Construct_UClass_AWorldItemDefault_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWorldItemDefault_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MyProject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWorldItemDefault_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WorldItemDefault.h" },
		{ "ModuleRelativePath", "WorldItemDefault.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWorldItemDefault_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWorldItemDefault>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWorldItemDefault_Statics::ClassParams = {
		&AWorldItemDefault::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWorldItemDefault_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWorldItemDefault_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWorldItemDefault()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWorldItemDefault_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWorldItemDefault, 1373993713);
	template<> MYPROJECT_API UClass* StaticClass<AWorldItemDefault>()
	{
		return AWorldItemDefault::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWorldItemDefault(Z_Construct_UClass_AWorldItemDefault, &AWorldItemDefault::StaticClass, TEXT("/Script/MyProject"), TEXT("AWorldItemDefault"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWorldItemDefault);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
